package id.lmnzr.example.geom.utils.dateserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class LocalDateTimeDeserializerTest {

    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
    }

    @SneakyThrows
    @Test
    public void testDeserialize() {
        String date = "{\"date\":\"2011-12-03T10:15:30+01:00[Europe/Paris]\"}";

        JsonParser parser = mapper.getFactory().createParser(date);
        parser.nextToken();
        parser.nextToken();
        parser.nextToken();
        DeserializationContext ctxt = mapper.getDeserializationContext();

        LocalDateTimeDeserializer deserializer = new LocalDateTimeDeserializer();

        LocalDateTime localDateTime = deserializer.deserialize(parser,ctxt);
        Assert.assertEquals(3,localDateTime.getDayOfMonth());
        Assert.assertEquals(12,localDateTime.getMonthValue());
        Assert.assertEquals(2011,localDateTime.getYear());
        Assert.assertEquals(15,localDateTime.getMinute());
        Assert.assertEquals(30,localDateTime.getSecond());
    }
}