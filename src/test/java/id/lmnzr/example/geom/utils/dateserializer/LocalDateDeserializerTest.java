package id.lmnzr.example.geom.utils.dateserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

public class LocalDateDeserializerTest {
    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
    }

    @SneakyThrows
    @Test
    public void testDeserialize() {
        String date = "{\"date\":\"2011-12-03T10:15:30+01:00[Europe/Paris]\"}";

        JsonParser parser = mapper.getFactory().createParser(date);
        parser.nextToken();
        parser.nextToken();
        parser.nextToken();
        DeserializationContext ctxt = mapper.getDeserializationContext();

        LocalDateDeserializer deserializer = new LocalDateDeserializer();

        LocalDate localDate = deserializer.deserialize(parser,ctxt);
        Assert.assertEquals(3,localDate.getDayOfMonth());
        Assert.assertEquals(12,localDate.getMonthValue());
        Assert.assertEquals(2011,localDate.getYear());
    }
}