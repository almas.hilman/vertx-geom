package id.lmnzr.example.geom.verticles.geometry.service;

import id.lmnzr.example.geom.AsyncMockHelper;
import id.lmnzr.example.geom.verticles.geometry.GeometryConfig;
import id.lmnzr.example.geom.verticles.geometry.model.Geometry;
import id.lmnzr.example.geom.verticles.geometry.service.eventbus.GeometryEventbus;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import java.util.ArrayList;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(VertxUnitRunner.class)
@PrepareForTest({JDBCClient.class})
@PowerMockIgnore("javax.management.*")
public class GeometryServiceTest {
    private final static String ID = "id";
    private final static String NAME = "name";
    private final static String DESCRIPTION = "description";
    private final static String FORMULA_AREA = "formula_area";
    private final static String FORMULA_PERIMETER = "formula_perimeter";
    private final static String SUCCESS = "success";
    private final static String DATA = "data";

    private EventBus eventBus;
    private JDBCClient mariaDb;

    @Before
    public void init() {
        Vertx vertx = Vertx.vertx();
        mariaDb = Mockito.mock(JDBCClient.class);
        PowerMockito.mockStatic(JDBCClient.class);
        PowerMockito.when(JDBCClient.createShared(Mockito.any(), Mockito.any())).thenReturn(mariaDb);
        eventBus = vertx.eventBus();
        new GeometryEventbus(vertx, mariaDb);

    }

    @Test
    public void health(TestContext context) {
        Async async = context.async();
        eventBus.request(GeometryConfig.HEALTH, "", handler -> {
            if(handler.succeeded()){
                context.assertEquals("Geometry service is working",handler.result().body().toString());
                async.complete();
            } else{
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void findById(TestContext context) {
        Async async = context.async();
        Long id = 1L;
        Geometry geometry = Geometry.valueOf(
            id,
            "square",
            "square",
            "r^2",
            "4*r"
        );
        JsonObject geometryJson = JsonObject.mapFrom(geometry);

        AsyncMockHelper.mockSuccesfulQuery(geometryJson, mariaDb);

        eventBus.request(GeometryConfig.FIND_BY_ID, id.toString(), handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(true, response.getBoolean(SUCCESS));
                context.assertEquals(geometry.getId(), response.getLong(ID));
                context.assertEquals(geometry.getName(), response.getString(NAME));
                context.assertEquals(geometry.getDescription(), response.getString(DESCRIPTION));
                context.assertEquals(geometry.getFormulaArea(), response.getString(FORMULA_AREA));
                context.assertEquals(geometry.getFormulaPerimeter(), response.getString(FORMULA_PERIMETER));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void findById_errorQuery(TestContext context) {
        Async async = context.async();
        Long id = 1L;

        AsyncMockHelper.mockFailedQuery(mariaDb);

        eventBus.request(GeometryConfig.FIND_BY_ID, id.toString(), handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(false, response.getBoolean(SUCCESS));
                context.assertEquals(AsyncMockHelper.ERROR_ASYNC_QUERY, response.getString("error"));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void findByName(TestContext context) {
        Async async = context.async();

        String name = "square";
        Geometry geometry = Geometry.valueOf(
            1L,
            name,
            "square",
            "r^2",
            "4*r"
        );
        JsonObject geometryJson = JsonObject.mapFrom(geometry);

        AsyncMockHelper.mockSuccesfulQuery(geometryJson, mariaDb);

        eventBus.request(GeometryConfig.FIND_BY_NAME, name, handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(true, response.getBoolean(SUCCESS));
                context.assertEquals(geometry.getId(), response.getLong(ID));
                context.assertEquals(geometry.getName(), response.getString(NAME));
                context.assertEquals(geometry.getDescription(), response.getString(DESCRIPTION));
                context.assertEquals(geometry.getFormulaArea(), response.getString(FORMULA_AREA));
                context.assertEquals(geometry.getFormulaPerimeter(), response.getString(FORMULA_PERIMETER));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void findByName_errorQuery(TestContext context) {
        Async async = context.async();
        String name = "square";

        AsyncMockHelper.mockFailedQuery(mariaDb);

        eventBus.request(GeometryConfig.FIND_BY_NAME, name, handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(false, response.getBoolean(SUCCESS));
                context.assertEquals(AsyncMockHelper.ERROR_ASYNC_QUERY, response.getString("error"));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void fetch(TestContext context) {
        Async async = context.async();
        Geometry square = Geometry.valueOf(
            1L,
            "square",
            "square",
            "r^2",
            "4*r"
        );
        Geometry circle = Geometry.valueOf(
            2L,
            "circle",
            "circle",
            "pi*r^2",
            "2*pi*r"
        );
        List<JsonObject> geometries = new ArrayList<>();
        geometries.add(JsonObject.mapFrom(square));
        geometries.add(JsonObject.mapFrom(circle));

        AsyncMockHelper.mockSuccesfulQuery(geometries, mariaDb);

        Integer limit = null;
        Integer offset = null;

        JsonObject req = new JsonObject();
        req.put("limit",limit);
        req.put("offset",offset);

        eventBus.request(GeometryConfig.FETCH, req.encode(), handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(true, response.getBoolean(SUCCESS));
                context.assertEquals(2,response.getJsonArray(DATA).size());
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void fetchPagination(TestContext context) {
        Async async = context.async();
        Geometry square = Geometry.valueOf(
            1L,
            "square",
            "square",
            "r^2",
            "4*r"
        );
        Geometry circle = Geometry.valueOf(
            2L,
            "circle",
            "circle",
            "pi*r^2",
            "2*pi*r"
        );
        List<JsonObject> geometries = new ArrayList<>();
        geometries.add(JsonObject.mapFrom(square));
        geometries.add(JsonObject.mapFrom(circle));

        Integer limit = 1;
        Integer offset = 0;

        JsonObject req = new JsonObject();
        req.put("limit",limit);
        req.put("offset",offset);

        AsyncMockHelper.mockSuccesfulQuery(geometries, mariaDb);

        eventBus.request(GeometryConfig.FETCH, req.encode(), handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(true, response.getBoolean(SUCCESS));
                context.assertEquals(2,response.getJsonArray(DATA).size());
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void fetch_errorQuery(TestContext context) {
        Async async = context.async();

        Integer limit = 1;
        Integer offset = 0;

        JsonObject req = new JsonObject();
        req.put("limit",limit);
        req.put("offset",offset);

        AsyncMockHelper.mockFailedQuery(mariaDb);

        eventBus.request(GeometryConfig.FETCH, req.encode(), handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(false, response.getBoolean(SUCCESS));
                context.assertEquals(AsyncMockHelper.ERROR_ASYNC_QUERY, response.getString("error"));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void delete(TestContext context) {
        Async async = context.async();

        Long id = 1L;
        AsyncMockHelper.mockSuccesfulUpdate(1, 1, mariaDb);

        eventBus.request(GeometryConfig.DELETE, id.toString(), handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(true, response.getBoolean(SUCCESS));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void delete_errorUpdate(TestContext context) {
        Async async = context.async();
        Long id = 1L;
        AsyncMockHelper.mockFailedUpdate(mariaDb);

        eventBus.request(GeometryConfig.DELETE, id.toString(), handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(false, response.getBoolean(SUCCESS));
                context.assertEquals(AsyncMockHelper.ERROR_ASYNC_UPDATE, response.getString("error"));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void save_newEntry(TestContext context) {
        Async async = context.async();

        Geometry square = Geometry.valueOf(
            null,
            "square",
            "square",
            "r^2",
            "4*r"
        );
        AsyncMockHelper.mockNotFoundQuery(mariaDb);
        AsyncMockHelper.mockSuccesfulUpdate(1, 1, mariaDb);

        eventBus.request(GeometryConfig.SAVE, JsonObject.mapFrom(square), handler -> {
            if (handler.succeeded()) {
                JsonObject response = new JsonObject(handler.result().body().toString());
                context.assertEquals(true, response.getBoolean(SUCCESS));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void save_updateExisting(TestContext context) {
        Async async = context.async();
        Geometry square = Geometry.valueOf(
            1L,
            "square",
            "square",
            "r^2",
            "4*r"
        );

        JsonObject geometryJson = JsonObject.mapFrom(square);

        AsyncMockHelper.mockSuccesfulQuery(geometryJson, mariaDb);
        AsyncMockHelper.mockSuccesfulUpdate(1, 1, mariaDb);

        eventBus.request(GeometryConfig.SAVE, JsonObject.mapFrom(square), handler -> {
            JsonObject response = new JsonObject(handler.result().body().toString());
            if (handler.succeeded()) {
                context.assertEquals(true, response.getBoolean(SUCCESS));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }

    @Test
    public void save_errorUpdate(TestContext context) {
        Async async = context.async();
        Geometry square = Geometry.valueOf(
            1L,
            "square",
            "square",
            "r^2",
            "4*r"
        );

        JsonObject geometryJson = JsonObject.mapFrom(square);

        AsyncMockHelper.mockSuccesfulQuery(geometryJson, mariaDb);
        AsyncMockHelper.mockFailedUpdate(mariaDb);

        eventBus.request(GeometryConfig.SAVE, JsonObject.mapFrom(square), handler -> {
            JsonObject response = new JsonObject(handler.result().body().toString());
            if (handler.succeeded()) {
                context.assertEquals(false, response.getBoolean(SUCCESS));
                context.assertEquals(AsyncMockHelper.ERROR_ASYNC_UPDATE, response.getString("error"));
                async.complete();
            } else {
                context.fail();
                async.complete();
            }
        });
    }
}