package id.lmnzr.example.geom.database.model;

import id.lmnzr.example.geom.AsyncMockHelper;
import id.lmnzr.example.geom.database.exception.DatabaseException;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(VertxUnitRunner.class)
@PrepareForTest({JDBCClient.class})
@PowerMockIgnore("javax.management.*")
public class EntityCrudRepositoryTest {
    private TestRepository testRepository;
    private JDBCClient mariaDb;

    @Before
    public void init() {
        mariaDb = Mockito.mock(JDBCClient.class);
        PowerMockito.mockStatic(JDBCClient.class);
        PowerMockito.when(JDBCClient.createShared(Mockito.any(), Mockito.any())).thenReturn(mariaDb);

        testRepository = new TestRepository(mariaDb);
    }

    @Test
    public void insertSuccessful(TestContext ctx) {
        Long insertedId = 10L;
        AsyncMockHelper.mockSuccesfulUpdate(insertedId, 1, mariaDb);

        testRepository.insert(new TestModel(), handler -> {
            if (handler.succeeded()) {
                ctx.assertEquals(insertedId, handler.result());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void insertFailed(TestContext ctx) {
        AsyncMockHelper.mockFailedUpdate(mariaDb);

        testRepository.insert(new TestModel(), handler -> {
            if (handler.failed()) {
                ctx.assertEquals(AsyncMockHelper.ERROR_ASYNC_UPDATE, handler.cause().getMessage());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void insertJsonSuccessful(TestContext ctx) {
        Long insertedId = 2L;
        AsyncMockHelper.mockSuccesfulUpdate(insertedId, 1, mariaDb);

        JsonObject json = JsonObject.mapFrom(new TestModel());
        testRepository.insert(json, (Handler<AsyncResult<Long>>) handler -> {
            if (handler.succeeded()) {
                ctx.assertEquals(insertedId, handler.result());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void insertJsonFailed(TestContext ctx) {
        AsyncMockHelper.mockFailedUpdate(mariaDb);

        JsonObject json = JsonObject.mapFrom(new TestModel());
        testRepository.insert(json, (Handler<AsyncResult<Long>>) handler -> {
            if (handler.failed()) {
                ctx.assertEquals(AsyncMockHelper.ERROR_ASYNC_UPDATE, handler.cause().getMessage());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findByIdSuccessful(TestContext ctx) {
        Long id = 5L;
        JsonObject expected = JsonObject.mapFrom(TestModel.valueOf(id));
        List<JsonObject> expectedList = new ArrayList<>();
        expectedList.add(expected);

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.findById(id, TestModel.class, handler -> {
            if (handler.succeeded()) {
                ctx.assertEquals(id, handler.result().getId());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findByIdEmpty(TestContext ctx) {
        Long id = 5L;
        List<JsonObject> expectedList = new ArrayList<>();

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.findById(id, TestModel.class, handler -> {
            if (handler.failed()) {
                ctx.assertEquals(DatabaseException.class, handler.cause().getClass());
                ctx.assertEquals("Data Not Found", handler.cause().getMessage());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findByIdFailed(TestContext ctx) {
        Long id = 5L;

        AsyncMockHelper.mockFailedQuery(mariaDb);
        testRepository.findById(id, TestModel.class, handler -> {
            if (handler.failed()) {
                ctx.assertEquals(AsyncMockHelper.ERROR_ASYNC_QUERY, handler.cause().getMessage());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findAllSuccessful(TestContext ctx) {
        Long id1 = 5L;
        Long id2 = 6L;
        JsonObject expected1 = JsonObject.mapFrom(TestModel.valueOf(id1));
        JsonObject expected2 = JsonObject.mapFrom(TestModel.valueOf(id2));
        List<JsonObject> expectedList = new ArrayList<>();
        expectedList.add(expected1);
        expectedList.add(expected2);

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.find(handler -> {
            if (handler.succeeded()) {
                ctx.assertEquals(id1, handler.result().getRows().get(0).getLong("id"));
                ctx.assertEquals(id2, handler.result().getRows().get(1).getLong("id"));
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findAllEmpty(TestContext ctx) {
        List<JsonObject> expectedList = new ArrayList<>();

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.find(handler -> {
            if (handler.succeeded()) {
                ctx.assertEquals(0, handler.result().getRows().size());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findAllPaginationSuccessful(TestContext ctx) {
        Long id1 = 5L;
        Long id2 = 6L;
        JsonObject expected1 = JsonObject.mapFrom(TestModel.valueOf(id1));
        JsonObject expected2 = JsonObject.mapFrom(TestModel.valueOf(id2));
        List<JsonObject> expectedList = new ArrayList<>();
        expectedList.add(expected1);
        expectedList.add(expected2);

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.find(2, 1, handler -> {
            if (handler.succeeded()) {
                ctx.assertEquals(id1, handler.result().getRows().get(0).getLong("id"));
                ctx.assertEquals(id2, handler.result().getRows().get(1).getLong("id"));
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findAllPaginationEmpty(TestContext ctx) {
        List<JsonObject> expectedList = new ArrayList<>();

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.find(2, 1, handler -> {
            if (handler.succeeded()) {
                ctx.assertEquals(0, handler.result().getRows().size());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findByMultipleKeySuccessful(TestContext ctx) {
        Long id1 = 5L;
        Long id2 = 6L;
        JsonObject expected1 = JsonObject.mapFrom(TestModel.valueOf(id1));
        JsonObject expected2 = JsonObject.mapFrom(TestModel.valueOf(id2));
        List<JsonObject> expectedList = new ArrayList<>();
        expectedList.add(expected1);
        expectedList.add(expected2);

        Map<String, Object> filter = new HashMap<>();
        filter.put(EntityCrudRepository.DELETED,0);

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.findByMultipleKey(filter,handler->{
            if (handler.succeeded()) {
                ctx.assertEquals(id1, handler.result().getRows().get(0).getLong("id"));
                ctx.assertEquals(id2, handler.result().getRows().get(1).getLong("id"));
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findByMultipleKeyEmpty(TestContext ctx) {
        List<JsonObject> expectedList = new ArrayList<>();

        Map<String, Object> filter = new HashMap<>();
        filter.put(EntityCrudRepository.DELETED,0);

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.findByMultipleKey(filter,handler->{
            if (handler.succeeded()) {
                ctx.assertEquals(0, handler.result().getRows().size());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findByMultipleKeyPaginationSuccessful(TestContext ctx) {
        Long id1 = 5L;
        Long id2 = 6L;
        JsonObject expected1 = JsonObject.mapFrom(TestModel.valueOf(id1));
        JsonObject expected2 = JsonObject.mapFrom(TestModel.valueOf(id2));
        List<JsonObject> expectedList = new ArrayList<>();
        expectedList.add(expected1);
        expectedList.add(expected2);

        Map<String, Object> filter = new HashMap<>();
        filter.put(EntityCrudRepository.DELETED,0);

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.findByMultipleKey(filter,2,1,handler->{
            if (handler.succeeded()) {
                ctx.assertEquals(id1, handler.result().getRows().get(0).getLong("id"));
                ctx.assertEquals(id2, handler.result().getRows().get(1).getLong("id"));
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void findByMultipleKeyPaginationEmpty(TestContext ctx) {
        List<JsonObject> expectedList = new ArrayList<>();

        Map<String, Object> filter = new HashMap<>();
        filter.put(EntityCrudRepository.DELETED,0);

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);
        testRepository.findByMultipleKey(filter,2,1,handler->{
            if (handler.succeeded()) {
                ctx.assertEquals(0, handler.result().getRows().size());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void query(TestContext ctx){
        Long id1 = 5L;
        Long id2 = 6L;
        JsonObject expected1 = JsonObject.mapFrom(TestModel.valueOf(id1));
        JsonObject expected2 = JsonObject.mapFrom(TestModel.valueOf(id2));
        List<JsonObject> expectedList = new ArrayList<>();
        expectedList.add(expected1);
        expectedList.add(expected2);

        AsyncMockHelper.mockSuccesfulQuery(expectedList, mariaDb);

        testRepository.executeQuery("SELECT * FROM test",handler->{
            if (handler.succeeded()) {
                ctx.assertEquals(id1, handler.result().getRows().get(0).getLong("id"));
                ctx.assertEquals(id2, handler.result().getRows().get(1).getLong("id"));
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void deleteByIdSuccessful(TestContext ctx){
        Long deletedId = 10L;
        AsyncMockHelper.mockSuccesfulUpdate(deletedId, 1, mariaDb);

        testRepository.deleteById(deletedId, handler -> {
            if (handler.succeeded()) {
                ctx.assertTrue(handler.result());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void deleteByIdNoneDeleted(TestContext ctx){
        Long deletedId = 10L;
        AsyncMockHelper.mockSuccesfulUpdate(deletedId, 0, mariaDb);

        testRepository.deleteById(deletedId, handler -> {
            if (handler.succeeded()) {
                ctx.assertFalse(handler.result());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void deleteByIdFailed(TestContext ctx){
        AsyncMockHelper.mockFailedUpdate(mariaDb);

        testRepository.deleteById(1L, handler -> {
            if (handler.failed()) {
                ctx.assertEquals(AsyncMockHelper.ERROR_ASYNC_UPDATE, handler.cause().getMessage());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void updateWithQuerySuccessful(TestContext ctx){
        int nUpdated = 1;

        AsyncMockHelper.mockSuccesfulUpdate(1L, nUpdated, mariaDb);
        testRepository.executeUpdate("UPDATE test SET is_deleted = 1 WHERE id = 1",handler->{
            if (handler.succeeded()) {
                ctx.assertEquals(1,handler.result().getUpdated());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void updateWithQueryFailed(TestContext ctx){
        AsyncMockHelper.mockFailedUpdate(mariaDb);
        testRepository.executeUpdate("UPDATE test SET is_deleted = 1 WHERE id = 1",handler->{
            if (handler.failed()) {
                ctx.assertEquals(AsyncMockHelper.ERROR_ASYNC_UPDATE, handler.cause().getMessage());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void updateSuccessful(TestContext ctx){
        int nUpdated = 1;
        Long id = 1L;
        AsyncMockHelper.mockSuccesfulUpdate(id, nUpdated, mariaDb);
        testRepository.update(TestModel.valueOf(id),handler->{
            if (handler.succeeded()) {
                ctx.assertTrue(handler.result());
            } else {
                ctx.fail();
            }
        });
    }

    @Test
    public void updateFailed(TestContext ctx){
        AsyncMockHelper.mockFailedUpdate(mariaDb);
        testRepository.update(TestModel.valueOf(1L),handler->{
            if (handler.failed()) {
                ctx.assertEquals(AsyncMockHelper.ERROR_ASYNC_UPDATE, handler.cause().getMessage());
            } else {
                ctx.fail();
            }
        });
    }

}