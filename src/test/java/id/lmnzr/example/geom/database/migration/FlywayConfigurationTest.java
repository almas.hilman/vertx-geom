package id.lmnzr.example.geom.database.migration;

import org.junit.Assert;
import org.junit.Test;

import java.net.URI;
import java.util.Map;


public class FlywayConfigurationTest {
    @Test
    public void getMigrationConfiguration() {
        Map<String, String> config = FlywayConfiguration.getConfiguration(URI.create("mysql://root@localhost:3306/testdb"));
        Assert.assertFalse(config.isEmpty());
        Assert.assertEquals(7,config.size());
        Assert.assertEquals("flyway_schema_history",config.get(FlywayConfiguration.FLYWAY_TABLE));
        Assert.assertEquals("database/migration",config.get(FlywayConfiguration.FLYWAY_LOCATION));
        Assert.assertEquals("jdbc:mysql://localhost:3306/testdb?useLegacyDatetimeCode=false&serverTimezone=Asia/Jakarta",config.get(FlywayConfiguration.FLYWAY_URL));
        Assert.assertEquals("root",config.get(FlywayConfiguration.FLYWAY_USER));
        Assert.assertEquals("0",config.get(FlywayConfiguration.FLYWAY_BASELINE_VERSION));
        Assert.assertEquals("true",config.get(FlywayConfiguration.FLYWAY_BASELINE_ON_MIGRATE));
        Assert.assertEquals("",config.get(FlywayConfiguration.FLYWAY_PASSWORD));
    }

}