package id.lmnzr.example.geom;

import static io.vertx.core.logging.LoggerFactory.LOGGER_DELEGATE_FACTORY_CLASS_NAME;

import id.lmnzr.example.geom.database.migration.FlywayConfiguration;
import id.lmnzr.example.geom.utils.EnvUtils;
import id.lmnzr.example.geom.verticles.geometry.GeometryVerticle;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import id.lmnzr.example.geom.verticles.rest.RestVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.logging.SLF4JLogDelegateFactory;
import org.flywaydb.core.Flyway;

/**
 * The type Main.
 */
public class Main {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        System.setProperty(LOGGER_DELEGATE_FACTORY_CLASS_NAME, SLF4JLogDelegateFactory.class.getName());
        Logger log = LoggerFactory.getLogger(Main.class);

        log.info("Start Deploying Applications");

        Vertx vertx = Vertx.vertx();

        vertx.deployVerticle(
            new GeometryVerticle(), deployHandler -> {
                if (deployHandler.succeeded()) {
                    log.info("Geometry Verticle Deployed");
                } else {
                    log.error(deployHandler.cause().getMessage());
                }
            });

        vertx.deployVerticle(
            new RestVerticle(), deployHandler -> {
                if (deployHandler.succeeded()) {
                    log.info("REST Verticle Deployed");
                } else {
                    log.error(deployHandler.cause().getMessage());
                }
            });

        Flyway flyway = Flyway.configure().configuration(
            FlywayConfiguration.getConfiguration(EnvUtils.getDatabaseURIfromEnv())).load();
        log.info("Run Flyway Database Migration");
        flyway.migrate();
    }
}
