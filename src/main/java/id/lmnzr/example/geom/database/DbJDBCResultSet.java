package id.lmnzr.example.geom.database;

import id.lmnzr.example.geom.database.exception.DatabaseException;
import id.lmnzr.example.geom.utils.PojoMapper;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.UpdateResult;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom JDBC Result class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DbJDBCResultSet {
    /**
     * Extract the generated / last inserted key from database update result
     * object. Note : the primary key field of the table must be placed in first
     * column
     *
     * @param source  the asynchronous result from JDBC update process
     * @param handler the asynchronous result handler
     */
    public static void getGeneratedKey(AsyncResult<UpdateResult> source, Handler<AsyncResult<Long>> handler) {
        if (source.succeeded()) {
            handler.handle(Future.succeededFuture(source.result().getKeys().getLong(0)));
        } else {
            handler.handle(Future.failedFuture(source.cause()));
        }
    }

    /**
     * Get status of database update process, return true if the update does not
     * error and affected row more than zero
     *
     * @param source  the asynchronous result from JDBC update process
     * @param handler the asynchronous result handler
     */
    public static void isUpdateSucceed(AsyncResult<UpdateResult> source, Handler<AsyncResult<Boolean>> handler) {
        if (source.succeeded()) {
            handler.handle(Future.succeededFuture(source.result().getUpdated() > 0));
        } else {
            handler.handle(Future.failedFuture(source.cause().getMessage()));
        }
    }

    /**
     * Convert result set first returned row to pojo. If the result set contains
     * no row, than the future in result handler will be marked as fail
     *
     * @param <T>       the type parameter
     * @param source    the asynchronous result set from JDBC update process
     * @param pojoClass the target pojo class
     * @param handler   the asynchronous result handler
     */
    public static <T> void toPojo(AsyncResult<ResultSet> source, Class<T> pojoClass, Handler<AsyncResult<T>> handler) {
        if (source.succeeded()) {
            if (source.result().getNumRows() > 0) {
                T pojo = PojoMapper.fromJsonObject(source.result().getRows().get(0), pojoClass);
                handler.handle(Future.succeededFuture(pojo));
            } else {
                handler.handle(Future.failedFuture(new DatabaseException("Data Not Found")));
            }
        } else {
            handler.handle(Future.failedFuture(source.cause()));
        }
    }

    /**
     * Convert result set first returned row to pojo list
     *
     * @param <T>       the type parameter
     * @param source    the asynchronous result set from JDBC update process
     * @param pojoClass the target pojo class
     * @param handler   the asynchronous result handler
     */
    public static <T> void toPojoList(AsyncResult<ResultSet> source, Class<T> pojoClass, Handler<AsyncResult<List<T>>> handler) {
        if (source.succeeded()) {
            List<T> result = new ArrayList<>();

            int numRows = source.result().getNumRows();
            for (int i = 0; i < numRows; i++) {
                result.add(PojoMapper.fromJsonObject(source.result().getRows().get(i), pojoClass));
            }

            handler.handle(Future.succeededFuture(result));
        } else {
            handler.handle(Future.failedFuture(source.cause()));
        }
    }

}
