package id.lmnzr.example.geom.database.driver;

import io.vertx.core.Vertx;
import io.vertx.ext.jdbc.JDBCClient;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.URI;

/**
 * Factory class for create JDBC Client instance
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ClientFactory {
    /**
     * Create non shared jdbc client based on given driver
     *
     * @param vertx       the vertx
     * @param databaseUri the database databaseUri
     * @param driver      the selected driver
     * @return the non shared jdbc client
     */
    public static JDBCClient createNonShared(Vertx vertx, URI databaseUri, ClientFactory.Driver driver) {
        if (driver == Driver.MARIADB) {
            return MariaDB.createNonShared(vertx, databaseUri);
        }
        return null;
    }

    /**
     * Create shared jdbc client based on given driver. The client data source
     * name will be the same as the database name
     *
     * @param vertx       the vertx
     * @param databaseUri the database databaseUri
     * @param driver      the selected driver
     * @return the shared JDBC Client
     */
    public static JDBCClient createShared(Vertx vertx, URI databaseUri, ClientFactory.Driver driver) {
        if (driver == Driver.MARIADB) {
            return MariaDB.createShared(vertx, databaseUri);
        }
        return null;
    }

    /**
     * The enum JDBC Driver.
     */
    public enum Driver {
        /**
         * Mysql driver.
         */
        MYSQL,
        /**
         * Mariadb driver.
         */
        MARIADB,
        /**
         * Postgresql driver.
         */
        POSTGRESQL,
        /**
         * Oracle driver.
         */
        ORACLE,
        /**
         * Sqlserver driver.
         */
        SQLSERVER
    }
}

