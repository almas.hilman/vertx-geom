package id.lmnzr.example.geom.database.query;

import id.lmnzr.example.geom.database.exception.DatabaseException;

/**
 * The type Select query generator.
 */
public class SelectQueryGenerator extends Generator {
    private final StringBuilder select;
    private Integer limit;
    private Integer offset;

    /**
     * Instantiates a new Select query generator.
     *
     * @param tableName the table name
     */
    public SelectQueryGenerator(String tableName) {
        super(tableName);

        this.select = new StringBuilder();
        this.limit = -1;
        this.offset = 0;
    }

    /**
     * Instantiates a new Select query generator.
     */
    public SelectQueryGenerator() {
        this("");
    }

    /**
     * From select query generator.
     *
     * @param tableName the table name
     */
    public void from(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Generate string.
     *
     * @return the string
     */
    public String generate() {
        StringBuilder generatedQuery = new StringBuilder();

        // Start select statement
        generatedQuery.append("SELECT ");

        if (this.select.toString().equals("") || this.select.toString().isEmpty()) {
            generatedQuery.append("*");
        } else {
            generatedQuery.append(this.select.toString());
        }
        // End of select statement

        // Start from statement
        if (this.tableName.equals("")) {
            throw new RuntimeException("Table name in from clause cannot be null or empty string");
        }

        generatedQuery.append(" FROM ").append(tableName);
        // End of from statement

        // Start where statement
        if (!this.where.toString().equals("") && !this.where.toString().isEmpty()) {
            generatedQuery.append(" WHERE ").append(this.where.toString());
        }
        // End of where statement

        if (this.limit != null && this.limit >= 0) {
            generatedQuery.append(" LIMIT ").append(limit);

            if (this.offset != null && this.offset >= 0) {
                generatedQuery.append(" OFFSET ").append(offset);
            }
        }

        return generatedQuery.toString();
    }

    /**
     * Limit select query generator.
     *
     * @param limit the limit
     */
    public void limit(Integer limit) {
        this.limit = limit;
    }

    /**
     * Offset select query generator.
     *
     * @param offset the offset
     */
    public void offset(Integer offset) {
        this.offset = offset;
    }
}

