package id.lmnzr.example.geom.utils.dateserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import id.lmnzr.example.geom.utils.DateUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

/**
 * The type Local date time deserializer.
 */
public class LocalDateTimeDeserializer extends StdDeserializer<LocalDateTime> {
    /**
     * Instantiates a new Date deserializer.
     */
    public LocalDateTimeDeserializer() {
        this(null);
    }

    /**
     * Instantiates a new Date deserializer.
     *
     * @param c the class
     */
    public LocalDateTimeDeserializer(Class<?> c) {
        super(c);
    }

    /**
     * Override deserialize process to convert iso date time string to GMT+7
     * date time string
     *
     * @param jsonParser             the json parser
     * @param deserializationContext the deserialization context
     * @return local date time string
     * @throws IOException the input output exception
     */
    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String dateString = jsonParser.getText().trim();

        LocalDateTime localDateTime;

        try {
            localDateTime = DateUtils.fromIsoString(dateString);
        } catch (DateTimeParseException ex) {
            try {
                localDateTime = DateUtils.fromMySqlString(dateString);
            } catch (DateTimeParseException ignored) {
                localDateTime = LocalDateTime.ofInstant(deserializationContext.parseDate(dateString).toInstant(),
                    DateUtils.getDefaultZoneId());
            }
        }

        return localDateTime;
    }
}
