package id.lmnzr.example.geom.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import id.lmnzr.example.geom.utils.dateserializer.LocalDateDeserializer;
import id.lmnzr.example.geom.utils.dateserializer.LocalDateSerializer;
import id.lmnzr.example.geom.utils.dateserializer.LocalDateTimeDeserializer;
import id.lmnzr.example.geom.utils.dateserializer.LocalDateTimeSerializer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * The type Pojo mapper.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PojoMapper {
    private static final Logger log = LoggerFactory.getLogger(PojoMapper.class);

    /**
     * Convert json object to selected pojo class
     *
     * @param <T>       the type parameter
     * @param source    the source
     * @param pojoClass the target pojo class
     * @return the pojo
     */
    public static <T> T fromJsonObject(JsonObject source, Class<T> pojoClass) {
        ObjectMapper objectMapper = getObjectMapper();

        T result = null;
        try {
            result = objectMapper.readValue(source.encode(), pojoClass);
        } catch (IOException exception) {
            log.debug(exception.getMessage());
        }

        return result;
    }

    /**
     * Convert pojo to hashmap
     *
     * @param source     the pojo source
     * @param removeNull the flag for ignore null value
     * @return the hashmap
     */
    public static Map<String, Object> toHashMap(Object source, Boolean removeNull) {
        Map<String, Object> result = new HashMap<>();

        JsonObject jsonObject = PojoMapper.toJsonObject(source, removeNull);
        jsonObject.iterator().forEachRemaining(objectEntry -> {
            if (objectEntry.getValue() != null || !removeNull) {
                result.put(objectEntry.getKey(), objectEntry.getValue());
            }
        });

        return result;
    }

    /**
     * Convert pojo to json object. Include null value
     *
     * @param source the pojo source
     * @return the json object
     */
    public static JsonObject toJsonObject(Object source) {
        return PojoMapper.toJsonObject(source, false);
    }

    /**
     * Convert pojo to json object
     *
     * @param source     the pojo source
     * @param removeNull the flag for ignore null value
     * @return the json object
     */
    public static JsonObject toJsonObject(Object source, Boolean removeNull) {
        ObjectMapper objectMapper = getObjectMapper();

        JsonObject result = new JsonObject();
        String jsonString;
        try {
            jsonString = objectMapper.writeValueAsString(source);
        } catch (JsonProcessingException e) {
            jsonString = "{}";
        }
        (new JsonObject(jsonString)).iterator().forEachRemaining(objectEntry -> {
            if (objectEntry.getValue() != null || !removeNull) {
                result.put(objectEntry.getKey(), objectEntry.getValue());
            }
        });

        return result;
    }

    private static ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer());
        javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());

        javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer());
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer());

        objectMapper.registerModule(javaTimeModule);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        return objectMapper;
    }
}
