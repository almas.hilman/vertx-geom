package id.lmnzr.example.geom.utils.dateserializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import id.lmnzr.example.geom.utils.DateUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * The type Local date serializer.
 */
public class LocalDateSerializer extends StdSerializer<LocalDate> {
    /**
     * Instantiates a new Date serializer.
     */
    public LocalDateSerializer() {
        this(null);
    }

    /**
     * Instantiates a new Date serializer.
     *
     * @param c the class
     */
    public LocalDateSerializer(Class<LocalDate> c) {
        super(c);
    }

    /**
     * Override serialize process to convert iso date object to GMT+7 date time
     * string
     *
     * @param localDate          the date
     * @param jsonGenerator      the json generator
     * @param serializerProvider the serializer provider
     * @throws IOException the input output exception
     */
    @Override
    public void serialize(LocalDate localDate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        LocalDateTime localDateTime = localDate.atTime(LocalTime.of(0, 0, 0));

        jsonGenerator.writeString(DateUtils.format(localDateTime, DateUtils.FORMAT_YMD));
    }
}