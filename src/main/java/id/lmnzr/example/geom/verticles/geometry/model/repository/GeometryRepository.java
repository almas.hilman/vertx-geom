package id.lmnzr.example.geom.verticles.geometry.model.repository;

import id.lmnzr.example.geom.database.DbJDBCResultSet;
import id.lmnzr.example.geom.database.exception.DatabaseException;
import id.lmnzr.example.geom.database.model.EntityCrudRepository;
import id.lmnzr.example.geom.verticles.geometry.model.Geometry;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.ext.jdbc.JDBCClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The type Geometry repository.
 */
public class GeometryRepository extends EntityCrudRepository {

    /**
     * The constant TABLE_NAME.
     */
    public static final String TABLE_NAME = "geometry";

    /**
     * The constant NAME.
     */
    public static final String NAME = "name";

    /**
     * The constant DESCRIPTION.
     */
    public static final String DESCRIPTION = "description";

    /**
     * The constant FORMULA_AREA.
     */
    public static final String FORMULA_AREA = "formula_area";

    /**
     * The constant FORMULA_PERIMETER.
     */
    public static final String FORMULA_PERIMETER = "formula_perimeter";

    /**
     * Instantiates a new Geometry repository.
     *
     * @param jdbcClient the jdbc client
     */
    public GeometryRepository(JDBCClient jdbcClient) {
        super(jdbcClient);
        tableName = TABLE_NAME;
    }

    /**
     * Find by id.
     *
     * @param id                 the id of geometry
     * @param asyncResultHandler the async result handler
     */
    public void findById(Long id, Handler<AsyncResult<Geometry>> asyncResultHandler) {
        Map<String, Object> filter = new HashMap<String, Object>() {{
            put(ID, id);
            put(DELETED, 0);
        }};

        this.findByMultipleKey(filter, findHandler -> DbJDBCResultSet.toPojo(findHandler, Geometry.class, asyncResultHandler));
    }

    /**
     * Find by name.
     *
     * @param name               the name of geometry
     * @param asyncResultHandler the async result handler
     */
    public void findByName(String name, Handler<AsyncResult<Geometry>> asyncResultHandler) {
        Map<String, Object> filter = new HashMap<String, Object>() {{
            put(NAME, name);
            put(DELETED, 0);
        }};

        this.findByMultipleKey(filter, findHandler -> DbJDBCResultSet.toPojo(findHandler, Geometry.class, asyncResultHandler));
    }

    /**
     * Find all not deleted geometry
     *
     * @param limit              maximum number of entries
     * @param offset             starting offset
     * @param asyncResultHandler the async result handler
     */
    public void fetch(Integer limit, Integer offset, Handler<AsyncResult<List<Geometry>>> asyncResultHandler) {
        Map<String, Object> filter = new HashMap<String, Object>() {{
            put(DELETED, 0);
        }};

        this.findByMultipleKey(filter, limit, offset, findHandler -> DbJDBCResultSet.toPojoList(findHandler, Geometry.class, asyncResultHandler));
    }

    /**
     * Find by id and id partner.
     *
     * @param geometry           the object geometry
     * @param asyncResultHandler the async result handler
     */
    public void save(Geometry geometry, Handler<AsyncResult<Long>> asyncResultHandler) {
        this.findById(geometry.getId(), finderHandler -> {
            if (finderHandler.succeeded()) {
                this.update(geometry, updateHandler -> {
                    if (updateHandler.succeeded()) {
                        if (updateHandler.result()) {
                            asyncResultHandler.handle(Future.succeededFuture(1L));
                        } else {
                            asyncResultHandler.handle(Future.failedFuture(new DatabaseException("No Data to Update")));
                        }
                    } else {
                        asyncResultHandler.handle(Future.failedFuture(updateHandler.cause()));
                    }
                });
            } else {
                if (finderHandler.cause().getMessage().equals("Data Not Found")) {
                    this.insert(geometry, asyncResultHandler);
                } else {
                    asyncResultHandler.handle(Future.failedFuture(finderHandler.cause()));
                }
            }
        });
    }

}