package id.lmnzr.example.geom.verticles.rest;

import id.lmnzr.example.geom.utils.EnvUtils;
import id.lmnzr.example.geom.verticles.rest.router.AppRouter;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * The type Rest verticle.
 */
public class RestVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(RestVerticle.class);

    @Override
    public void start(Promise<Void> startPromise) {
        AppRouter appRouter = new AppRouter(vertx);

        int port = EnvUtils.getPortFromEnv();

        vertx.createHttpServer()
            .requestHandler(appRouter.getRouter())
            .listen(port, http -> {
                if (http.succeeded()) {
                    startPromise.complete();
                    log.debug("HTTP server started on port " + port);
                } else {
                    startPromise.fail(http.cause());
                }
            });
    }

    @Override
    public void stop() {
        log.debug("Rest Verticle stopped");
    }
}
